﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DemoDateAzure.Startup))]
namespace DemoDateAzure
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
